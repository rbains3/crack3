#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "md5.h"
#include "binsearch.h"

const int PASS_LEN=50;        // Maximum any password can be
const int HASH_LEN=33;        // Length of MD5 hash strings


// Given a hash and a plaintext guess, return 1 if
// the hash of the guess matches the given hash.
// That is, return 1 if the guess is correct.
char* tryguess(char *hash, char **guess, int size)
{
    // Compare the two hashes
    char *rv = binsearch(guess, size, hash);
    return rv;
}

int compare(const void *a,const void *b)
{
    return strcmp(*(char **)a,*(char **)b);
    
}


// TODO
// Read in the hash file and return the array of strings.
// Use the technique we showed in class to expand the
// array as you go.
char **read_hashes(char *filename)
{
    FILE *f;
    f = fopen(filename,"r");
    if(!f)
    {
        printf("Could not open file!");
        exit(1);
    }
    char **m = malloc(400 * sizeof(char*));//using malloc to allocate memory
    
    int totalSize = 0;
    int i = 0;
    char temp[1000];
    
    while(fgets(temp,1000, f) != NULL)
    {
        if(i == totalSize)
        {
            totalSize += 400;
            
            char **newm = (char**)realloc(m, totalSize * sizeof(char *));
            
            if(newm == NULL)
            {
                exit(1);
            }
            else if(newm != m)
            {
                m = newm;
            }
        }
 
        int len = strlen(temp);
        m[i] = (char*)malloc((len + 1) * sizeof(char));
        strcpy(m[i], temp);
        i++;
    }
    
    fclose(f);
    
    if(i == totalSize)
    {
        char **newm = (char**)realloc(m, (1 + totalSize) * sizeof(char *));
        
        if(newm == NULL)
        {
        exit(1);
        }
        else if(newm != m)
        {
            m = newm;
        }
    }
    m[i + 1] = NULL;
    
    return m;
}


// TODO
// Read in the dictionary file and return the data structure.
// Each entry should contain both the hash and the dictionary
// word.
char **read_dict(char *filename, int *size)
{
    FILE *f;
    f = fopen(filename,"r");
    if(!f)
    {
        printf("Could not open file!");
        exit(1);
    }
    char **m = malloc(200*sizeof(char*));//using malloc to allocate memory
    
    int totalSize=0;
    int i = 0;
    char temp[1000];
    
    while(fgets(temp,1000, f) != NULL)
    {
        if(i == totalSize)
        {
            totalSize += 200;
            
            char **newm = (char**)realloc(m, totalSize * sizeof(char *));
            
            if(newm == NULL)
            {
                exit(1);
            }
            else if(newm != m)
            {
                m = newm;
            }
        }
        
        int len = strlen(temp);
        m[i] = (char*)malloc((len + 37) * sizeof(char));
        strcpy(m[i], md5(temp,strlen(temp)-1));
        strcat(m[i]," ");
        strcat(m[i], temp);
        i++;
    }
    
    fclose(f);
    
    if(i == totalSize)
    {
        char **newm = (char**)realloc(m, (1 + totalSize) * sizeof(char *));
        
        if(newm == NULL)
        {
        exit(1);
        }
        else if(newm != m)
        {
            m = newm;
        }
    }
    
    m[i + 1] = NULL;
    *size = i;
    return m;
}


int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    // TODO: Read the hash file into an array of strings
    char **hashes = read_hashes(argv[1]);

    // TODO: Read the dictionary file into an array of strings
    int dictSize = 0;
    int *dictSizeP = &dictSize;
    char **dict = read_dict(argv[2], dictSizeP);
    
    // TODO: Sort the hashed dictionary using qsort.
    // You will need to provide a comparison function.
    qsort(dict, dictSize, sizeof(char *), compare);

    // TODO
    // For each hash, search for it in the dictionary using
    // binary search.
    // If you find it, get the corresponding plaintext dictionary
    // entry. Print both the hash and word out.
    // Need only one loop. (Yay!)
    int i = 0;
    while(hashes[i] != NULL)
    if(strcmp(tryguess(hashes[i], dict, dictSize), "-1") != 0)
    {
        printf("%s", dict[i]);
        i++;
    }
}
